import React, { Component } from 'react';
import { Container, Row, Col, Card, CardBody, CardHeader, ButtonGroup, Button } from 'reactstrap'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      weekday:[
        {
          id: '',
          day: "Lunes",
        },{
          id: '',
          day: "Martes",
        },{
          id: '',
          day: "Miercoles",
        },{
          id: '',
          day: "Jueves",
        },{
          id: '',
          day: "Viernes",
        },{
          id: '',
          day: "Sabado",
        },
      ],
      selectday:[],
      index: 0
    }
  }

  handleClickAdd = (w) => e => {
    this.setState({
      selectday:[
        ...this.state.selectday,
        {
          id: this.state.index,
          day: w.day,
        }
      ],
      index: this.state.index+1
    })
  }

  selectdayDelete(i, e) {
    this.state.selectday.splice(this.state.selectday.indexOf(this.state.selectday[i]),1)
    this.setState({
      selectday:[
        ...this.state.selectday,
      ]
    })
  }

  render() {

    const { weekday, selectday } = this.state

    return (
      <Container fluid={true}>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Agregar
              </CardHeader>
              <CardBody>
                <ButtonGroup>
                  {weekday.map( (w, id) =>{
                    return <Button onClick={this.handleClickAdd(w)} key={id} color="info">
                      {w.day}
                    </Button>
                  })}
                </ButtonGroup>
              </CardBody>
            </Card>
            <br />
            {selectday.map ( (sd, i) =>
              <Card key={i}>
                <CardHeader>
                  Todos los {sd.day}
                </CardHeader>
                <CardBody>
                  <Button onClick={this.selectdayDelete.bind(this, i)} color="danger">
                    Eliminar
                  </Button>
                </CardBody>
              </Card>
            )}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
