const ADD_TWIT = 'twit/add'
const LIKE_TWIT = 'twit/like'
const DELETE_TWIT = 'twit/delete'

export const addTwit = payload => ({
  type: ADD_TWIT,
  payload,
})

export const like = payload => ({
  type: LIKE_TWIT,
  payload,
})

export const del = payload => ({
  type: DELETE_TWIT,
  payload,
})

const initialState = {
  data: [],
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TWIT: {
      return {
        ...state,
        data: state.data.concat(action.payload),
      }
    }
    case LIKE_TWIT: {
      return {
        ...state,
        data: state.data.map(x => x.id === action.payload ? {
          ...x,
          likes: !x.likes ? 1 : x.likes + 1,
        } : x)
      }
    }
    case DELETE_TWIT: {
      return {
        ...state,
        data: state.data.filter(x => x.id !== action.payload),
      }
    }
    default:
      return state
  }
}
