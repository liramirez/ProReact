import React, { Component } from 'react'
import { connect } from 'react-redux'
import Card from './Card'
import { addTwit, like, del } from './Twits'

const styles = {
  container: {
    minHeight: 'calc(100vh - 20px)',
    width: 'calc(100vw - 30px)',
    backgroundColor: '#eee',
    padding: '10px 15px',
  }
}

class App extends Component {
  state = {
    form: {
      twit: '',
    }
  }

  handleChange = field => evt =>
    this.setState({ form: {
      ...this.state.form,
      [field]: evt.target.value,
    } })

  handleSubmit = e => {
    e.preventDefault()
    const { addTwit } = this.props
    const { form } = this.state
    addTwit({ ...form, id: Math.random().toString(36) })
    e.target.reset()
  }

  render() {
    const { like, addTwit, del, data } = this.props

    return (
      <div style={styles.container}>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Ingresa twit"
            onChange={this.handleChange('twit')}
          />
        </form>
        <Card>
          <ul>
            {data.map(x => <li key={x.id}>
              {x.twit}
              <button onClick={() => like(x.id)}>Like {x.likes}</button>
              <button onClick={() => del(x.id)}>Delete</button>
            </li>)}
          </ul>
        </Card>

      </div>
    )
  }
}

const mapStateToProps = state => {
  const { Twits } = state

  return Twits
}
const mapDispatchToProps = dispatch => ({
  addTwit: payload => dispatch(addTwit(payload)),
  like: id => dispatch(like(id)),
  del: id => dispatch(del(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App)
