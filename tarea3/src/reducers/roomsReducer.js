const initialState = {
  roomsList: [],
}

export default function(state = initialState, action) {
  console.log("ACTION: ", action.type)
  console.log("PAYLOAD :", action.payload)
  console.log("\n");

  switch (action.type) {
    case "ADD_ROOM":

      return{
        roomsList: [...state.roomsList, {name: action.payload, selected: false}]
      }
    default:
      return state
  }
}
