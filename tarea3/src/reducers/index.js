import { combineReducers } from "redux";
import RoomsReducer from './roomsReducer'

const rootReducer = combineReducers({
  RoomsReducer
});

export default rootReducer;
