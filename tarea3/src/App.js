import React, { Component } from 'react';
import RoomsForm from './components/roomsForm'
import RoomsList from './components/roomsList'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-body">
                <RoomsForm />
                <br/>
                <RoomsList />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
