export function addRoom(room) {
  //console.log("ACTION: AddRoom")
  return{
    type: "ADD_ROOM",
    payload: room
  }
}
