import React, { Component } from "react";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { addRoom } from '../actions/roomActions'

class RoomsForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      roomsList:[],
      roomsForm:{
        name:'',
        selected:'',
      }
    }
  }

  handleChange = field => e => {
    this.setState({
      roomsForm: {
        ...this.state.roomsForm,
        [field]: e.target.value,
      }
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    e.target.reset()

    if (this.state.roomsForm.name !== null){
      this.props.addRoom(this.state.roomsForm.name)
      this.setState({
        roomsForm: {
          name: null,
        }
      })
    }
  }

  render() {
    console.log(this.state)

    return(
      <div className="card">
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group row">
              <label className="col-sm-2 col-form-label">Sala :</label>
              <div className="col-sm-10">
                <input
                  onChange={this.handleChange('name')}
                  type='text'
                  className="form-control"
                  placeholder='Ingrese Sala'
                />
              </div>
            </div>
            <button
              type='submit'
              className="btn btn-success float-right">
              Agregar
            </button>
          </form>
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addRoom,
  }, dispatch);
}

export default connect(
  null,
  mapDispatchToProps
)(RoomsForm)
