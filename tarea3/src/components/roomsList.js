import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

class RoomsList extends Component{
  render() {
    console.log("roomsList", this.props.roomsList);

    return(
      <ul className="list-group">
        {this.props.roomsList.map((room, i) => {
          return(
            <li
              className="list-group-item"
              key ={i}>
              {room.name}
            </li>
          )
        })}
      </ul>
    )
  }
}
function mapStateToProps(state) {
  return {
    roomsList: state.RoomsReducer.roomsList,
  };
}

export default connect(
  mapStateToProps,
  null,
)(RoomsList)
