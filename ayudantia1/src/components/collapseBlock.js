import React, { Component } from 'react';

export default class TabsBlock extends Component {

  constructor(props) {
    super(props)
    this.state = {
      message:'Hola',
    }
  }

  handleClick = e => {

  }

  render() {

    const {message} = this.state

    return(
      <div className="card">
        <div className="card-body">
          <p>
            <button onClick={this.handleClick()} type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
              Colapse
            </button>
          </p>
          <div className="collapse" id="collapseExample">
            <div className="card card-body">
              {message}
            </div>
          </div>

        </div>
      </div>
    )
  }
}
