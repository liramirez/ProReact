import React, { Component } from 'react';

export default class TabsBlock extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tabs: [
        {
          id: 0,
          title: 'Primero',
          message: 'Hola',
        },{
          id: 1,
          title: 'Segundo',
          message: 'Chao',
        },
      ],
      text:'',
    }
  }

  handleClick = (id) => e => {
    const {tabs} = this.state
    const [x] = tabs.filter((tab) => {
      return (tab.id === id)
    })
    console.log(id, x)
    this.setState({text:x.message})

  }


  render(){

    const {tabs, text} = this.state

    return(
      <div className="card">
        <div className="card-body">
          <ul className="nav nav-tabs">
            {tabs.map((t, id) =>
              <li key={id} className="nav-item">
                <a className="nav-link" onClick={this.handleClick(t.id)} href="#">{t.title}</a>
              </li>
            )}
            {text}
          </ul>
        </div>
      </div>
    )
  }
}
