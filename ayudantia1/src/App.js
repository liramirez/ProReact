import React, { Component } from 'react';
import './App.css';
import TabsBlock from './components/tabsBlock'
import CollapseBlock from './components/collapseBlock'

class App extends Component {

  render() {

    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-body">
                  <TabsBlock />
                  <br/>
                  <CollapseBlock />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
