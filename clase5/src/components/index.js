import NewsForm from './NewsForm'
import NewsFeed from './NewsFeed'
import CategoriesList from './CategoriesList'
import Card from './Card'
import UserForm from './UserForm'

export {
  NewsFeed,
  NewsForm,
  CategoriesList,
  Card,
  UserForm,
}
