import React from 'react'

const styles = {
  input:{
    
  }
}

export default class Input extends React.Component {
  render() {
    const { input } = this.props
    return (
      <input
        style={style.input}
        {..this.props}
        value={input.value}
        onChange={e => input.onChange(e.target.value)}
    )
  }
}
