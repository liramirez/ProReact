import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { Input } from './Elements'

export default class UserForm extends React.Component {
  render() {
    const { handleSubmit } = this.props
    return (
      <form onSubmit={handleSubmit}>
        <Field name='name' component={Input} type='text' placeholder='Nombre' />
        <Field name='lastname' component={Input} type='text' placeholder='Apellido' />
        <Field name='age' component={Input} type='number' placeholder='Edad' />
        <input type='submit' value='Enviar' />
      </form>
    )
  }
}

export default reduxForm({
  form: 'userForm',
})(UserForm)
