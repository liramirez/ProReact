import React, { Component } from 'react'
import { connect } from 'react-redux'
import { UserForm } from './components'
import * as usersActions from './ducks/Users'

const styles = {
  container: {
    background: '#efefef',
    display: 'flex',
    flexFlow: 'column wrap',
    height: '100vh',
  },
  row: {
    display: 'flex',
    flexFlow: 'row wrap',
  },
  column: width => ({
    display: 'flex',
    flexFlow: 'column wrap',
    justifyContent: 'space-evenly',
    flex: width,
  }),
}

class App extends Component {
  /*
  enviar = async values =>{  //handleSubmit
    const result = await axios.post('/users', values)
    console.log(result)
  }
  */

  render() {
    const { createUser } = this.props

    return (
      <div style={styles.container}>
        <Userform onSubmit={this.enviar}/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch => bindActionCreators({
  ...usersActions,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App)
