const initialState = {
  userList: [],
  fruitList:[],
}

export default function(state = initialState, action) {
  console.log("ACTION: ", action.type)
  console.log("PAYLOAD :", action.payload)
  console.log("\n");

  switch (action.type) {
    case "ADD_USER":
      return{
        userList: [...state.userList, action.payload]
      }

    case "DELETE_USER":
      state.userList.splice(state.userList.indexOf(action.payload),1)
      return{
        userList: [...state.userList]
      }

    case "GET_FRUIT":
      return state

    case "GET_FRUIT_FULFILLED":
      return{
        fruitList: action.payload.data,
      }

    case "GET_FRUIT_REJECTED":
      return{
        fruitList: [],
      }
    default:
      return state
  }
}
