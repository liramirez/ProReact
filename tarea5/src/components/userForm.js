import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, Label, Card, CardBody, CardHeader, Input } from 'reactstrap'
import { Form, reduxForm, Field } from 'redux-form'
import { addUser } from '../actions/userActions'

class UserForm extends React.Component {

  submit = (user) => {
    console.log("User: ", user);
    this.props.addUser(user)
  }

  render() {
    const { handleSubmit } = this.props
    
    return (
      <Card>
        <CardHeader>
          Formulario Alumnos
        </CardHeader>
        <CardBody>
          <Form onSubmit={handleSubmit(this.submit)}>
            <Label for='user'>
                Nombre Usuario
            </Label>
            <Input tag={Field} type='text' name='user' component='input' />
            <Button type='submit'>Enviar</Button>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

UserForm = reduxForm({
  form: 'userForm',
})(UserForm)

function mapStateToProps(state){
  return {}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    addUser,
}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserForm)
