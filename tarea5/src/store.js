import { createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import reducers from './reducers';

export default (applyMiddleware(promiseMiddleware())(createStore))(reducers);
