import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { UserForm } from './components'
import { Button, Row, Col, Container, Card, CardBody, CardHeader, ListGroup, ListGroupItem } from 'reactstrap'
import { deleteUser, getFruit } from './actions/userActions'

class App extends Component {
  componentDidMount(){
    this.props.getFruit()
  }

  constructor(props) {
    super(props)
    this.state = {
      roomsList:[
        {
          room_name: "cowork",
          users: [
            {user_name: "pedro"},
            {user_name: "juan"},
            {user_name: "diego"},
          ]
        },
      ],
    }
  }

  userDelete(i, e){
    this.props.deleteUser(this.props.userList[i])
  }

  render() {
    console.log("this.props :", this.props);
    return (
      <Container fluid={true}>
        <Row>
          <Col>
            <UserForm />
          <br/>
            <Card>
              <CardHeader>
                Listado Alumnos
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {this.props.fruitList.map( (user, i) =>{
                    return <ListGroupItem key={i}>
                        {user.name}
                        <Button className="float-right" onClick={this.userDelete.bind(this, i)}>Eliminar</Button>
                      </ListGroupItem>
                  })}
                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state){
  return {
    userList: state.UserReducer.userList,
    fruitList: state.UserReducer.fruitList,
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    deleteUser,
    getFruit,
}, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App)
