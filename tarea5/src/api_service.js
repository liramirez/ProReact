import axios from 'axios';
import store from './store';

let REACT_APP_BACKEND_URL = 'http://localhost:3001'


export function get(url) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${store.getState().UserReducer.token}`;
  return axios.get(REACT_APP_BACKEND_URL + url);
}

export function post(url,body) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${store.getState().UserReducer.token}`;
  return axios.post(REACT_APP_BACKEND_URL + url,body);
}

export function put(url, body) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${store.getState().UserReducer.token}`;
  return axios.put(REACT_APP_BACKEND_URL + url,body);
}

export function del(url, body) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${store.getState().UserReducer.token}`;
  return axios.delete(REACT_APP_BACKEND_URL + url,body);
}
