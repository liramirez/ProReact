import * as api from '../api_service'

export function addUser(user) {
  //console.log("ACTION: AddUser")
  return{
    type: "ADD_USER",
    payload: user
  }
}

export function deleteUser(user) {
  //console.log("ACTION: DeleteUser")
  return{
    type: "DELETE_USER",
    payload: user
  }
}

export function getFruit() {
  const request = api.get('/fruits/')
  return {
    type: "GET_FRUIT",
    payload: request
  };
}
