const Events = (function(){
  var topics = {}
  var hOP = topics.hasOwnProperty

  //on === subscribe
  //emit === publish

  return {
    on: function(topic, listener) {
      if(!hOP.call(topics, topic)) topics[topic] = []
      topics[topic].push(listener)
    },
    emit: function(topic, info) {
      if(!hOP.call(topics, topic)) return
      topics[topic].forEach(function(item) {
      	item(info != undefined ? info : {})
      })
    }
  }
})()

Events.on('add-todo', todo => {
  Todos.add(todo)
  Render.notifyAll()
})

const Render = (() => { //antes se llamaba Subject
  let data = []

  return{
    suscribe: observer => data.push(observer),
    notifyAll: () => data.forEach(x => x())
  }
})()


const Todos = (() => {
  let data = []

  return {
    add: x => data.push(x),
    get: () => data,
  }
})()

Render.suscribe(() => {   //recive una funcion
  const todoList = Todos.get()
  const todosTemplate = document.getElementById('todo-list')
  const template = todoList.reduce((acc, el) =>
    `${acc}<li>${el.title}</li>`, '')
  todosTemplate.innerHTML = template
})

window.onload = () => {
  const button = document.getElementById('todo-submit')
  button.onclick = () => {
    const todoInput = document.getElementById('todo-input')
    const todoValue = todoInput.value
    todoInput.value = ''
    const todo = {
      id: Math.random().toString(36) + Math.random().toString(36),
      title: todoValue,
    }
    Events.emit('add-todo', todo)   //Todos.add(todo)
    //Render.notifyAll()
  }
}

/*
  add -> agrega elementos a data
  get -> obtiene elementos de data
  window.onload -> window hace referencia al html, onload solo se ejecuta cuando se carga el html
  document.getElementById('todo-submit') -> se busca el elemento boton por su Id
  button.onclick -> se le agrega un manejador
  todoInput = document.getElementById('todo-input') -> buscar valor del campo y agregar al listado
  id: Math.random().toString(36) -> genera un id, el math se repite dos veces para que no se repita, el largo es de 36 cada cuando
  Todos.add(todo) -> se agrega el todo al modulo de Todos
  Todos.get() -> devuelve todos los todos que se encuentran
  Todo y Subject tienen data, pero no son el mismo data
  suscribe: observer => data.push(observer) -> define al observador como una funcion
  notifyAll: () => data.forEach(x => x()) -> Itera cada uno de los objetos dentro de data y lsos ejecuta
  var events = (function(){ -> define un objeto que va a contener todos los eventos a los que suscribo
  subscribe: function(topic, listener) -> topico es el evento que le voy a pasar, listener es una funcion que se agrega denro de los topicos
  puedo tener cuantas funciones suscritas yo quiera a un topico
  publish: function(topic, info) -> recibe topicos y datos, si tiene el topico ejecuta todas las funciones suscritas

  Events.on('loaded', x => {
    console.log('Cargado!', x)
  })

  Events.emit('loaded',{ a: 1, b: 2})




*/
