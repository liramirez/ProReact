import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addHero } from './redux/heroes'
import { bindActionCreators } from 'redux'
import HeroesList from './components/HeroesList'
import HeroForm from './components/HeroForm'
import './App.css'

class App extends Component {
  constructor(props){
    super(props)

    this.state = {
      hero:{
        name: null,
        age: null,
        race: null,
        weapon: null,
      },
      pristine: true,
      errors: null,
      showError: false,
    }
  }

  onChangeInput = (e, field) => {
    const value = e.target.value
    const {hero} = this.state
    this.setState({
      hero:{
        ...hero,
        [field]: value
      }
    })
  }

  handleSubmit = (e) =>{
    const {hero} = this.state

    let showError = false
    const errors = {}

    if (!hero.name) {
      showError =true
      errors.name = 'Name is required'
    }
    if (!hero.age) {
      showError =true
      errors.age = 'age is required'
    }
    if (!hero.race) {
      showError =true
      errors.race = 'race is required'
    }
    if (!hero.weapon) {
      showError =true
      errors.weapon = 'weapon is required'
    }
    if (!showError) {
      this.props,addHero(hero)

      this.setState =({
        hero:{
          name: null,
          age: null,
          race: null,
          weapon: null,
        },
        pristine: true,
        errors: null,
        showError: false,
      })
    } else {
      this.setState({
        errors,
        showError
      })
    }

    e.preventDefault()
  }

  render() {
    console.log(this.state.hero)
    const {heroes} = this.props
    const {hero, errors, showError} = this.state
    return (
      <div className="container">
        <div className="heroes-list">
          <HeroesList
            heroes={heroes}
          />
        </div>
        <div className="form">
          <HeroForm
            onChangeInput={this.onChangeInput}
            handleSubmit={this.handleSubmit}
            hero={hero}
            errors={errors}
            showError={showError}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({ heroes: state.heroes });

const mapDispatchToProps = dispatch => bindActionCreators({ addHero }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)
