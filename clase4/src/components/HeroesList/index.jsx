import React from 'react'
import './style.css'

export default ({heroes}) => <div className="list">
  {heroes.map((e, index) => {
    return <div className="item" key={index}>
      <div>
        name: {e.name}
      </div>
      <div>
        age: {e.age}
      </div>
      <div>
        race: {e.race}
      </div>
      <div>
        weapon: {e.weapon}
      </div>
    </div>
  })}
</div>
