import React from 'react'
import './style.css'

export default ({onChangeInput, handleSubmit, hero, errors, showError}) =>
<form className="formly" onSubmit={handleSubmit}>
  <div className="title">New Hero</div>
  <div className="form-row">
    <div>Name</div>
    <input type="text" value={hero.name || ''} onChange={(e) => onChangeInput(e, 'name')}/>
    <div className='row-error'>
      {showError && errors.name}
    </div>
  </div>

  <div className="form-row">
    <div>Age</div>
    <input type="text" value={hero.age || ''} onChange={(e) => onChangeInput(e, 'age')}/>
    <div className='row-error'>
      {showError && errors.age}
    </div>
  </div>

  <div className="form-row">
    <div>Race</div>
    <input type="text" value={hero.race || ''} onChange={(e) => onChangeInput(e, 'race')}/>
    <div className='row-error'>
      {showError && errors.race}
    </div>
  </div>

  <div className="form-row">
    <div>Weapon</div>
    <input type="text" value={hero.weapon || ''} onChange={(e) => onChangeInput(e, 'weapon')}/>
    <div className='row-error'>
      {showError && errors.weapon}
    </div>
  </div>
  
  <button type="submit">Sign up</button>

</form>
