const ADD_HERO = 'add-hero'

export const addHero = (hero) => ({
  type: ADD_HERO,
  payload: hero
})

const initialState = [
  {
    name: 'Frodo',
    race: 'Hobbit',
    age: '50',
    weapon: 'dagger'
  }
]

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_HERO: {
      return [
        ...state,
        action.payload
      ]
    }

    default: {
      return state
    }
  }
}
