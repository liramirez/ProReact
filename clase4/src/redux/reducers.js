import heroes from './heroes/index'
import { reducer as formReducer } from 'redux-form'

export default {
  heroes,
  form: formReducer
}
