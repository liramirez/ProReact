import { combineReducers } from "redux";
import ItemReducer from './itemReducer'

const rootReducer = combineReducers({
  ItemReducer
});

export default rootReducer;
