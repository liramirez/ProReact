import React, { Component } from 'react';
import './App.css';
import { loadItems } from './actions/itemActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class App extends Component {
  componentDidMount(){
    this.props.loadItems()
  }

  render() {
    console.log(this.props);
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-body">
                {this.props.itemList.map(i => i.name)}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state){
  return{
    itemList: state.ItemReducer.itemList,
    isItemLoaded: state.ItemReducer.isItemLoaded,
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    loadItems,
  }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(App)
