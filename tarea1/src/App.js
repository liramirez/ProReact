import React, { Component } from 'react';     //Importar React y sus Componentes
import './App.css';
import NewsForm from './components/newsForm' //NewsForm se exporta por defecto, importar mas de una cosa desde un archivo
import Menu from './components/menu'

class App extends Component {                 //Componente llamado App

  render() {                      /* metodo que devuelve la vista del componente */

    /*console.log(this.state.form) */
    return (                      /* devuelve la vista */
      <div className="App">
        <Menu />
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-body">
                  <NewsForm />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;                 //Exportar el componente para reutilizarlo cuando se necesite
