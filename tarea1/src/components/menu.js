import React, { Component } from 'react';     //Siempre debe ir cuando se crean un nuevo componente

class Menu extends Component{       //Define la clase Componente Menu

  render() {                                          //Render de la vista
    return(                                           //Return de la vista
      <nav className="navbar navbar-dark bg-dark">
        <span className="navbar-text text-white">
          NewsFeed
        </span>
      </nav>
    )
  };
};

export default Menu         //Exporta la clase Componente Menu
