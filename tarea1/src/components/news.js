import React, { Component } from 'react';     //Siempre debe ir cuando se crean un nuevo componente
import AnswerForm from './answerForm';

export default class News extends Component {     //Se Exporta y se define el Componente NewsForm

  render() {
    const {user, profile_picture, message} = this.props

    //console.log("NEWS STATE :", this.state);
    //console.log("NEWS PROPS :", this.props);
    //console.log("\n");

    return(
        <div className="card noticia">
          <div className="card-body">
            <div className="media">
              <img className="mr-3" src={"https://picsum.photos/100/100/?image="+user.charCodeAt()} />
              <div className="media-body">
                <h5 className="mt-0">{user}</h5>
                {message}
              </div>
            </div>
            <AnswerForm />
          </div>
        </div>
    )
  }
}
