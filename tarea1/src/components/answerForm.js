import React, { Component } from 'react';     //Siempre debe ir cuando se crean un nuevo componente
import Answer from './answer'

export const validate = ({user, profile_picture, message}) => {
  const errors = []
  if (!user){
    errors.push('Usuario requerido')
  }
  /*if (!profile_picture){
    errors.push('Foto requerida')
  }*/
  if (!message){
    errors.push('Comentario es requerido')
  }
  return errors
}

export default class AnswerForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      errors: [],
      answers:[],
      answerForm:{
        user: '',
        profile_picture:'',
        message:'',
      },
      message: '',
    }
  }

  handleChange = field => e => {
    this.setState({
      answerForm: {
        ...this.state.answerForm,
        [field]: e.target.value,  /*valor que tiene el campo en ese momento */
      }
    })
  }

  handleSubmit = e => {

    const {answerForm} = this.state

    e.preventDefault()
    e.target.reset()      // limpia input del formulario
    this.setState({message: ''})
    const errors =validate(answerForm)
    this.setState({errors})     //this.setState actualiza el estado, errors tiene los valores que reemplazaran a los antiguos
    if (errors.length > 0){
      return
    }
    this.setState({message:'Enviado...'})
    setTimeout(() =>
      this.setState({ message: 'Mensaje enviado con exito'}), 1000)

    this.setState ({
      answers: [
        ...this.state.answers,
        {user:answerForm.user , profile_picture:answerForm.profile_picture, message:answerForm.message},
      ]
    })
  }

  render() {

    return(
      <div>
        <div className="row">
          <div className="col-11 offset-1">
            <br/>
            {this.state.answers.map((a, id) =>
              <div>
                <Answer key={id} user={a.user} profile_picture={a.profile_picture} message={a.message} />
                <br/>
              </div>
            )}
          </div>
        </div>
        <div className="card formulario">
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              {/*<div className="form-group row">
                <label className="col-sm-2 col-form-label">Foto</label>
                <div className="col-sm-10">
                  <input onChange={this.handleChange('profile_picture')} type='number' placeholder='Foto' className="form-control"/>
                </div>
              </div>*/}
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Usuario</label>
                <div className="col-sm-10">
                  <input onChange={this.handleChange('user')} type='text' placeholder='Usuario' className="form-control"/>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Comentario</label>
                <div className="col-sm-10">
                  <input onChange={this.handleChange('message')} type='text-area' placeholder='Comentario' className="form-control"/>
                </div>
              </div>
              <button type='submit' className="btn btn-success float-right">Responder</button>
            </form>
            <p>{this.state.message}</p>
            <ul>
              {this.state.errors.map((x, i) => <li key={i}>{x}</li>)}
            </ul>
          </div>
        </div>

      </div>
    )
  }

};
