import React, { Component } from 'react';     //Siempre debe ir cuando se crean un nuevo componente
import News from './news'

export const validate = ({user, profile_picture, message}) => {         //Se exporta y se define el constructor
  const errors = []
  if (!user){
    errors.push('Usuario requerido')
  }
  /*if (!profile_picture){
    errors.push('Foto equerida')
  }*/
  if (!message){
    errors.push('Comentario es requerido')
  }
  return errors
}

export default class NewsForm extends Component {     //Se Exporta y se define el Componente NewsForm

  constructor(props) {
    super(props)
    this.state = {
      errors: [],
      news:[
        {
          user: 'Lorem ipsum chileno',
          profile_picture: 1,
          message: 'Lorem ipsum dolor sit cuchuflí barquillo bacán jote gamba listeilor po cahuín, luca melón con vino pichanga coscacho ni ahí peinar la muñeca chuchada al chancho achoclonar. Chorrocientos pituto ubicatex huevo duro bolsero cachureo el hoyo del queque en cana huevón el año del loly hacerla corta impeque de miedo quilterry la raja longi ñecla. Hilo curado rayuela carrete quina guagua lorea piola ni ahí.',
        },{
          user: 'Lorem ipsum',
          profile_picture: 2,
          message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },{
          user: 'Cicero 45 a.C.',
          profile_picture: 3,
          message: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. ',
        },
      ],
      newsForm:{
        user: '',
        profile_picture:'',
        message:'',
      },
      message: '',
    }
  }

  componentDidMount(){
  }

  handleChange = field => e => {
    this.setState({
      newsForm: {
        ...this.state.newsForm,
        [field]: e.target.value,  /*valor que tiene el campo en ese momento */
      }
    })
  }

  handleSubmit = e => {

    const {newsForm} = this.state

    e.preventDefault()
    e.target.reset()    // limpia input del formulario
    this.setState({message: ''})
    const errors =validate(newsForm)
    this.setState({errors})     //this.setState actualiza el estado, errors tiene los valores que reemplazaran a los antiguos
    if (errors.length > 0){
      return
    }
    this.setState({message:'Enviado...'})
    setTimeout(() =>
      this.setState({ message: 'Mensaje enviado con exito'}), 1000)

    this.setState ({
      news: [
        ...this.state.news,
        {user:newsForm.user , profile_picture:newsForm.profile_picture, message:newsForm.message},
      ]
    })
  }

  render() {

    const {message, errors, news} = this.state

    return(
      <div>
        <div className="card">
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              {/*<div className="form-group row">
                <label className="col-sm-2 col-form-label">Foto</label>
                <div className="col-sm-10">
                  <input onChange={this.handleChange('profile_picture')} type='number' placeholder='Foto' className="form-control"/>
                </div>
              </div>*/}
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Usuario</label>
                <div className="col-sm-10">
                  <input onChange={this.handleChange('user')} type='text' placeholder='Usuario' className="form-control"/>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">Comentario</label>
                <div className="col-sm-10">
                  <input onChange={this.handleChange('message')} type='text-area' placeholder='Comentario' className="form-control"/>
                </div>
              </div>
              <button type='submit' className="btn btn-success float-right">Enviar</button>
            </form>
            <p>{message}</p>
            <ul>
              {errors.map((x, i) => <li key={i}>{x}</li>)}
            </ul>
          </div>
        </div>
        <br/>
        {news.map((n, id) =>
          <div>
            <News key={id} user={n.user} profile_picture={n.profile_picture} message={n.message} />
            <br/>
          </div>
        )}
      </div>
    )
  }
}
//onChange={this.handleChange('profile_picture')}
