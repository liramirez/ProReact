import React, { Component } from 'react';     //Siempre debe ir cuando se crean un nuevo componente

export default class Answer extends Component {

  render() {
    const {user, profile_picture, message} = this.props

    //console.log("Answer STATE :", this.state);
    //console.log("Answer PROPS :", this.props);
    //console.log("\n");

    return(
        <div className="card respuesta">
          <div className="card-body">
            <div className="media">
              <img className="mr-3" src={"https://picsum.photos/100/100/?image="+user.charCodeAt()} />
              <div className="media-body">
                <h5 className="mt-0">{user}</h5>
                {message}
              </div>
            </div>
          </div>
        </div>
    )
  }
}
