import React, { Component } from 'react'
import logo from './logo.svg'
import Card from './Card'
import data from './data'
import Form from './Form'

const styles = {
  container: {
    minHeight: 'calc(100vh - 20px)',
    width: 'calc(100vw - 30px)',
    backgroundColor: '#eee',
    padding: '10px 15px',
  }
}

class App extends Component {
  state = {
    form: {
      user: 'Chanchito feliz',
      profile_picture: 'https://twistedsifter.files.wordpress.com/2015/08/happy-pig-smiling.jpg?w=300',
      message: '',
    },
    data: data,
  }

  handleChange = field => evt =>
    this.setState({
      form: {
        ...this.state.form,
        [field]: evt.target.value,
      }
    })
  
  handleSubmit = evt => {
    evt.preventDefault()
    evt.target.reset()
    const { form } = this.state
    setTimeout(() => {
      this.setState({
        data: [form].concat(this.state.data),
        form: {
          ...this.state.form,
          message: '',
        },
      })
    }, 500)
  }
  
  render() {
    return (
      <div style={styles.container}>
        <Card>
          <Form
            handleSubmit={this.handleSubmit}
            handleChange={this.handleChange}
          />
        </Card>
        {this.state.data.map(x =>
          <Card>
            <h3>{x.user}</h3>
            <img src={x.profile_picture} width={50} height={50} />
            <p>{x.message}</p>
            <ul>
              {x.answers && x.answers.map(y =>
                <li>
                  <h5>{y.user}</h5>
                  <img src={y.profile_picture} width={50} height={50} />
                  <p>{y.message}</p>
                </li>
              )}
            </ul>
          </Card>)}
      </div>
    )
  }
}

export default App
