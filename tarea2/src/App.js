import React, { Component } from 'react';
import './App.css';
import TodoForm from './components/todoForm'
import TodoList from './components/todoList'

class App extends Component {
  render() {

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-body">
                <TodoForm />
                <br/>
                <TodoList />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
