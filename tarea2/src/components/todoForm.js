import React, { Component } from "react";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { addTodo } from '../actions/todoActions'

class TodoForm extends Component{

  constructor(props) {
    super(props)
    this.state = {
      todoList:[],
      todoForm:{
        item: '',
        complete:'',
      }
    }
  }

  handleChange = field => e => {
    //console.log("e.t.v",e.target.value)
    this.setState({
      todoForm: {
        ...this.state.todoForm,
        [field]: e.target.value,
      }
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    //console.log("e",e)
    e.target.reset()

    //console.log("item", item)
    /*
    const {add} = this.props
    const {todoForm} = this.state

    add({ ...todoForm, id: Math.random().toString(36)})
    */
    if (this.state.todoForm.item !== null){
      this.props.addTodo(this.state.todoForm.item)
      this.setState({
        todoForm: {
          item: null,
        }
      })
    }
  }

  render() {
    //console.log(this.state)

    return(
      <div>
        <div className="card">
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group row">
                <label className="col-sm-2 col-form-label">ToDo</label>
                <div className="col-sm-10">
                  <input
                    onChange={this.handleChange('item')}
                    type='text'
                    className="form-control"
                    placeholder='Ingrese item del To Do List'
                  />
                </div>
              </div>
              <button
                type='submit'
                className="btn btn-success float-right">
                Agregar
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
/*
const mapDispatchToProps = dispatch => ({
  addTodo: payload => dispatch(addTodo(payload))
})
*/

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addTodo,
  }, dispatch);
}

export default connect(
  null,
  mapDispatchToProps
)(TodoForm)
