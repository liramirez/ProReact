import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { selectTodo } from '../actions/todoActions'
import { deleteTodo } from '../actions/todoActions'
import { filterTodo } from '../actions/todoActions'

class TodoList extends Component{

  todoComplete(i, e){
    this.props.selectTodo(this.props.todoList[i])
  }

  todoDelete(i, e){
    //console.log("i", i);
    //console.log("this.props.todoList[i]", this.props.todoList[i]);
    this.props.deleteTodo(this.props.todoList[i])
  }

  render() {
    console.log("todoList", this.props.todoList);
    //console.log("this.props", this.props);
    //const {data} = this.props.todoList
    return (
      <div>
      <button
        className="btn btn-success">
        Filtrar
      </button>
      <ul className="list-group">
        {this.props.todoList.map((todo, i) => {
          //console.log("this", this);
          //console.log("i", i);
          return(
            <li
              className="list-group-item"
              key ={i}>
              {todo.desc} {todo.complete}
              { (todo.complete)?
                <button
                  onClick={this.todoComplete.bind(this, i)}
                  className="btn btn-primary float-right">
                  Completo
                </button>
                :
                <button
                  onClick={this.todoComplete.bind(this, i)}
                  className="btn btn-danger float-right">
                  No Completo
                </button>
              }
              <button
                onClick={this.todoDelete.bind(this, i)}
                className="btn btn-warning float-right">
                Eliminar
              </button>
            </li>
          )
        })}
      </ul>
    </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    todoList: state.TodoReducer.todoList,

    selectTodo: state.TodoReducer.selectTodo,
    deleteTodo: state.TodoReducer.deleteTodo,
    filterTodo: state.TodoReducer.filterTodo,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    selectTodo,
    deleteTodo,
    filterTodo,
  }, dispatch);
}

/*
const mapDispatchToProps = dispatch => ({
  add: payload => dispatch(add(payload))
})
*/

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TodoList)
