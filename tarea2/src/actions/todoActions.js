export function addTodo(todo) {
  //console.log("ACTION: AddTodo")
  return{
    type: "ADD_TODO",
    payload: todo
  }
}

export function selectTodo(todo) {
  //console.log("ACTION: SelectTodo")
  return{
    type: "SELECT_TODO",
    payload: todo,
  }
}

export function deleteTodo(todo) {
  //console.log("ACTION: DeletTodo")
  return{
    type: "DELETE_TODO",
    payload: todo,
  }
}

export function filterTodo(todo) {
  //console.log("ACTION: FilterTodo")
  return{
    type: "FILTER_TODO",
    payload: todo,
  }
}
