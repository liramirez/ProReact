const initialState = {
  todoList: [],
}

export default function(state = initialState, action) {
  console.log("ACTION: ", action.type)
  console.log("PAYLOAD :", action.payload)
  console.log("\n");

  const elementTodo = state.todoList[state.todoList.findIndex(x => x.desc === action.payload.desc)]
  const indexOfTodo = state.todoList.indexOf(elementTodo)

  switch (action.type) {
    case "ADD_TODO":

      return{
        todoList: [...state.todoList, {desc: action.payload, complete: false}]
      }
    case "SELECT_TODO":
      elementTodo.complete = !elementTodo.complete

      return{
        todoList: [...state.todoList]
      }
    case "DELETE_TODO":
      //console.log("state.todoList", state.todoList);
      //console.log("indexOfTodo", indexOfTodo);

      state.todoList.splice(indexOfTodo, 1);

      return{
        todoList: [...state.todoList]
      }
    case "FILTER_TODO":
      
      return{

      }
    default:
      return state
  }
}
