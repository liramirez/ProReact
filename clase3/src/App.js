import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NewsForm, CategoriesList, NewsFeed, Card } from './components'
import { bindActionCreators } from 'redux'
import * as categoriesActions from './ducks/Categories'
import * as newsActions from './ducks/News'

const styles = {
  container: {
    background: '#efefef',
    display: 'flex',
    flexFlow: 'column wrap',
    height: '100vh',
  },
  row: {
    display: 'flex',
    flexFlow: 'row wrap',
  },
  column: width => ({
    display: 'flex',
    flexFlow: 'column wrap',
    justifyContent: 'space-evenly',
    flex: width,
  }),
}

class App extends Component {
  render() {
    const {categories, news, selectCategory, addNews, selectedCategory} = this.props

    return (
      <div style={styles.container}>
        <div style={styles.row}>
          <div style={styles.column(1)}></div>
          <div style={styles.column(2)}>
            <Card>
              <NewsForm
                handleSubmit={addNews}
                categoryId={selectedCategory}
              />
            </Card>
          </div>
        </div>
        <div style={styles.row}>
          <div style={styles.column(1)}>
            <Card>
              <CategoriesList
                categories={categories}
                handleClick={selectCategory}
              />
            </Card>
          </div>
          <div style={styles.column(2)}>
            <Card>
              <NewsFeed news={news} />
            </Card>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const {Categories: {data: categories}} = state
  const {News: {data:news}} = state

  const selectCategory = categories.find(x => x.selected)
  const filteredNews = news.filter(x =>
    x.categoryId === selectCategory.id)

  return {
    selectCategory,
    categories,
    news: filteredNews,
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  ...categoriesActions,
  ...newsActions,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App)
