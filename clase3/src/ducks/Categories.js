function mac(type, ...argNames){
  return function (...args){

    return action
  }
}

const mod = module => type => '${module}/${type}'
const t = mod('category')
const SELECT = t('select')
const ADD = t('add')
const DEL = t('del')

/*
const SELECT = 'category/select'
const ADD = 'category/add'
const DEL = 'category/del'

export const selectCategory = payload => ({
  type: SELECT,
  payload, //id
})

export const addCategory = payload => ({
  type: ADD,
  payload, //id
})

export const delCategory = payload => ({
  type: DEL,
  payload, //id
})
*/

export const selectCategory = mac(SELECT, 'payload')

export const addCategory = mac(ADD, 'payload')

export const delCategory = mac(DEL, 'payload')

const initialState = {
  data:[
    {id: 1, name:'Defecto', selected: true},
    {id: 2, name:'Deporte', selected: false},
    {id: 3, name:'Noticias', selected: false},
  ],
}

export default function reducer(state = initialState, action){
  switch(action.type){
    case SELECT:{
      return{
        ...state,
        data: state.data.map(x => ({
          ...x,
          selected: x.id === action.payload,
        }))
      }
    }
    case ADD:{
      return{
        ...state,
        data: state.data.concat(action.payload),
      }
    }
    case DEL:{
      return{
        ...state,
        data: state.data.concat(action.payload),
      }
    }
    default:
      return state
  }
}
