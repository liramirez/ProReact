import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Form, {validate} from './Form' //Form se exporta por defecto , importar mas de una cosa desde un archivo

class App extends Component {
  state = {
    errors: [],
    form:{
      name: '',
      lastname:'',
      email:'',
    },
    message: ''
  }

  handleChange = field => e => {
    this.setState({
      form: {
        ...this.state.form,
        [field]: e.target.value, //valor que tiene el campo en ese momento
      }
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    //console.log(this.state.form)
    this.setState({message: ''})
    // const {name, lastname, email} = this.state.form
    //const errors = []
    const errors =validate(this.state.form)
    this.setState({errors})
    if (errors.length > 0){
      return
    }
    this.setState({message:'Enviado...'})
    setTimeout(() =>
      this.setState({ message: 'Formulario enviado con exito'}), 1000)
  }

  render() {
    const {errors, message} = this.state
    //console.log(this.state.form)
    return (
      <div className="App">
        <Form
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
        />
        <p>{message}</p>
        <ul>
          {errors.map((x, i) => <li key={i}>{x}</li>)}
        </ul>
      </div>
    );
  }
}

export default App;
