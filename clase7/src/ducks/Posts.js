import { reduceReducers } from 'ducks-maker'
import { crudHOD } from './hods'

const mod = crudHOD('posts')

export default mod.default

const url = 'https://jsonplaceholder.typicode.com/posts'

export const fetchPosts = mod.fetch(url)
export const add = mod.add()
export const del = mod.del
