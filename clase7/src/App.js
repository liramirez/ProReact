import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import * as postsActions from './ducks/Posts'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import Client from './containers/Client'

class App extends Component {
  constructor(props){
    super(props)
    const { fetchPosts } = props
    fetchPosts()
  }

  render() {
    return (
      <div className="App">
        <Route exact path='/' component={Client} />
        <Route exact path='/admin' component={Admin} />
      </div>
    );
  }
}

const mapStateToProps = state => state
const mapDispatchToProps = dispatch =>
  bindActionCreators(postsActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)
