import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux'
import * as userDuck from './ducks/Users'

class App extends Component {
  constructor(props){
    super(props)
    const (fetchUsers) = props
    fetchUsers()
  }

  render() {
    const { Users: {data: users, fetching }} = this.props
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          {fetching && 'Cargando...'}
          {Object.key(users).map( x => {
            const user = users[x]

            return <li key={x}>{user.name}</li>
          })}
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => state
const mapDispatchToProps = dispatch =>
  binActionCreator(userDuck, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App)
