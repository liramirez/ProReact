export const makeFetch = ({ START, SUCCESS, ERROR }) => ({
  [START]: state => ({ ...state, fetching: true }),
  [SUCCESS]: (state, { payload }) => ({
    ...state,
    data: { ...state.data, ...payload },
    fetched: true,
    fetching: false,
  }),
  [ERROR]: (state, { error }) => ({
    ...state,
    error,
    fetching: false,
  })
})
