import { fetchUsers } from './Users'

describe ('Users', () => {
  describe('fetchUsers', () => {
    it.skip('handle error', async () => {

    })

    it('fetches users', async () => {
      const dispatch = jest.fn()
      const getState = jest.fn()
      const resolvedValue = () => ({
        json: () => Promise.resolve([
          { id: 1, name: 'Chanchito'},
          { id: 2, name: 'Feliz'},
        ])
      })
      const fetch = jest.fn().mockImplementation(resolvedValue)

      await fetchUsers()(dispatch, getState, {fetch})
      expect(fetch.mock.calls).toEqual([
        ['https://jsonplaceholder.typicode.com/users'],
      ])
      expect(dispatch.mock.calls).toEqual([
        [{ type: 'users/fetch-start'}],
        [{ payload: {
          "1": { id:1, name:"Chanchito"},
          "2": { id:2, name:"Feliz"},
        }}]
      ])

    })
  })
})
