import { makeTypes, asyncmac, createReducer } from 'ducks-maker'
import { makeFetch } from './hors'

const t = makeTypes('users')
const FETCH = t('fetch').async()

const fetchActions = asyncMac(FETCH)

const initialState = {
  data:{},
}

export default createReducer(initialState, makeFetch(FETCH))

export const fetchUsers = () =>
  async (dispatch, _, { fetch }) => {
    dispatch(fetchActions.start())
    const data = await fetch('https://jsonplaceholder.typicode.com/users')
    const parsed = await data.json()
    const obj = parsed.reduce((acc, el) => {
      acc[el.id] = el
      return acc
    }, {})

    dispatch(fetchActions.success(obj))
  }
